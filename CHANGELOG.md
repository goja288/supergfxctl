# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [4.0.4] - 2022-02-05
### Changed
- Adjust the kernel cmdline arg code path

## [4.0.3] - 2022-02-04
### Added
- Add config option `no_logind`: Don't use logind to see if all sessions are
  logged out and therefore safe to change mode. This will be useful for people not
  using a login manager, however it is not guaranteed to work unless all graphical
  sessions are ended and nothing is hooking the drivers. Ignored if `always_reboot`
  is set.
- Add config option `logout_timeout_s`: The timeout in seconds to wait for all user
  graphical sessions to end. Default is 3 minutes, 0 = infinite. Ignored if
  `no_logind` or `always_reboot` is set.
- Add new dbus method: `PendingMode`, to check if a mode change is required
- Add new dbus method: `PendingUserAction`, to check if the user is required to perform an action
- Add new dbus method: `Config`, to get the current base config
- Add new dbus method: `SetConfig`, to set the base config
- Add `-p, --pend-action` CLI arg to get the pending user action if any
- Add `-P, --pend-mode` CLI arg to get the pending mode change if any`
- Add ability to read `supergfxd.mode=` from kernel cmdline on startup and set the mode appropriately
### Removed
- CLI option `--force` was unused, it is now removed.

## [4.0.2] - 2022-01-22
### Changed
- Adjust how xorg config is created so that EGPU mode uses it also

## [4.0.1] - 2022-01-20
### Changed
- Fix version upgrade of config
- Recreate the config if parsing fails
- Only write the mode change to config file, don't update live config
### Added
- AMD dedicated + hybrid config for xorg
- "AllowExternalGpus" added to xorg for Nvidia Egpu mode

## [4.0.0] - 2022-01-18
### Added
- Add new dbus method: `Version` to get supergfxd version
- Add new dbus method: `Vendor` to get dGPU vendor name
- Add new dbus method: `Supported` to get list of supported modes
- Add `-v, --version` CLI arg to get supergfxd version
- Add `-V, --vendor` CLI arg to get dGPU vendor name
- Add `-s, --supported` CLI arg to get list of supported modes
- Add new config option: `vfio_save` to reload VFIO on boot
- Add new config option: `compute_save` to reload compute on boot
- Add new config option: `always_reboot` reboot to change modes
### Changed
- Adjust startup to check for ASUS eGPU and dGPU enablement if no modes supported
- If nvidia-drm.modeset=1 is set then save mode and require a reboot by default\
- Add extra check for Nvidia dGPU (fixes Flow 13")
- Properly check the correct device for power status
### Breaking
- Rename Vendor, GetVendor to Mode, GetMode to better reflect their results

## [3.0.0] - 2022-01-10
### Added
- Keep a changelog
### Changed
- Support laptops with AMD dGPU
  + `hybrid`, `integrated`, `vfio` only
  + Modes unsupported by AMD dGPU will return an error
- `nvidia` mode is now `dedicated`
- Don't write the config twice on laptops with hard-mux switch
- CLI print zbus error string if available
- Heavy internal cleanup and refactor to make the project a bit nicer to work with